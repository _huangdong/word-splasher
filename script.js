let isPaused = false;
let timeout;
let words;
let index = 0;
let interval = 150; // Default interval
let wordPositions = [];
let simpleWords = ['a', 'an', 'and', 'at', 'be', 'by', 'it', 'its', 'in', 'on', 'of', 'to', 'that', 'the'];
let stopwatchInterval;
let startTime;

// Update the word count display when text changes
document.getElementById('textInput').addEventListener('input', function() {
    let wordCount = this.value.split(/\s+/).filter(Boolean).length;
    document.getElementById('wordCount').innerText = `Word Count: ${wordCount}`;
});

document.getElementById('intervalSlider').addEventListener('input', function() {
    interval = +this.value;
    document.getElementById('intervalLabel').innerText = `Interval: ${interval}ms`;
});

document.getElementById('resetButton').addEventListener('click', function() {
    if (isPaused) {
        index = 0;
        document.getElementById('wordLabel').innerText = "Word will appear here";
        document.getElementById('startButton').innerText = "Start"; // Change button text to "Start"
        isPaused = false; // Update paused state

        clearInterval(stopwatchInterval);
        document.getElementById('stopwatch').innerText = "Time: 0.0s";
    }
});

document.getElementById('startButton').addEventListener('click', function() {
    if (this.innerText === "Start") {
        startStopwatch();
        let text = document.getElementById('textInput').value;
        words = text.split(/\s+/).filter(Boolean); // Re-initialize words
        calculateWordPositions(text, words);
        index = 0; // Reset index
        this.innerText = "Pause";
        displayNextWord();
    } else if (this.innerText === "Pause") {
        this.innerText = "Resume";
        isPaused = true;
        clearTimeout(timeout);
        clearInterval(stopwatchInterval); // Pause the stopwatch
        document.getElementById('resetButton').disabled = false; // Enable reset button when paused
    } else if (this.innerText === "Resume") {
        let text = document.getElementById('textInput').value;
        words = text.split(/\s+/).filter(Boolean); // Re-initialize words
        calculateWordPositions(text, words);
        this.innerText = "Pause";
        isPaused = false;
        displayNextWord();
        startStopwatch(); // Resume the stopwatch
    }
});

function calculateWordPositions(text, words) {
    wordPositions = [];
    let currentIndex = 0;
    words.forEach(word => {
        let wordLength = word.length;
        let wordStart = text.indexOf(word, currentIndex);
        let wordEnd = wordStart + wordLength;
        wordPositions.push({ start: wordStart, end: wordEnd });
        currentIndex = wordEnd;
    });
}

function highlightWordInParagraph() {
    if (index < words.length) {
        let paragraphsText = document.getElementById('textInput').value;
        let beforeWord = paragraphsText.substring(0, wordPositions[index].start);
        let afterWord = paragraphsText.substring(wordPositions[index].end);
        let word = paragraphsText.substring(wordPositions[index].start, wordPositions[index].end);

        document.getElementById('paragraphs').innerHTML = 
            beforeWord + 
            `<span class="highlight">${word}</span>` +
            afterWord;
    }
}

function displayNextWord() {
    if (index < words.length && !isPaused) {
        let word = words[index].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,""); // Strips punctuation
        document.getElementById('wordLabel').innerText = word;
        highlightWordInParagraph();
        index++;
        let isSimpleWord = simpleWords.indexOf(word) > -1;
        let adjustedInterval = isSimpleWord ? (interval - 50) : (interval + 50);
        if (adjustedInterval < 0) adjustedInterval = 10;
        timeout = setTimeout(displayNextWord, adjustedInterval); // Use dynamic interval

        if (index === words.length - 1) { // Check if it's the last word
            clearInterval(stopwatchInterval);
        }
    }
    else {
        document.getElementById('resetButton').disabled = false; // Enable reset button when paused
        isPaused = true;
    }
}

function startStopwatch() {
    if (!startTime) { // Start timer if not already started
        startTime = new Date();
    }
    let currentTime = new Date();
    let timeElapsedBeforePause = (currentTime - startTime) / 1000;
    stopwatchInterval = setInterval(function() {
        let updatedTime = new Date();
        let totalElapsedTime = (updatedTime - startTime) / 1000;
        document.getElementById('stopwatch').innerText = `Time: ${(totalElapsedTime - timeElapsedBeforePause).toFixed(1)}s`;
    }, 100);
}

function updateStopwatch() {
    let currentTime = new Date();
    let timeElapsed = (currentTime - startTime) / 1000;
    document.getElementById('stopwatch').innerText = `Time: ${timeElapsed.toFixed(1)}s`;
}
